require 'sinatra/base'

class SinatraApp < Sinatra::Base
  get '/' do
    'Hello World!'
  end

  get '/quote' do
    "Excellence prospers in the absence of excuses - Lorii Myers"
  end

  get '/hello/:name' do |name|
    "Hello #{name.capitalize}!"
  end
end
