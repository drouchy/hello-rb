# Hello Rb

Very polite Ruby app. Says hello all the time.

Now can say your name.

# How to setup

```shell
bundle install
```

# How to run

```shell
bundle exec rackup
```
