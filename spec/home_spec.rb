require 'rspec'
require 'rack/test'
require_relative '../app.rb'

describe 'Sinatra App' do
  include Rack::Test::Methods

  def app
    SinatraApp.new
  end

  it "says hello" do
    get '/'

    expect(last_response.body).to include("Hello World!")
  end

  it "says hello to a specific person" do
    get '/hello/Bob'

    expect(last_response.body).to include("Hello Bob!")
  end

  it "capitalise the name of the person" do
    get '/hello/bob'

    expect(last_response.body).to include("Hello Bob!")
  end
end
