" - Lorii Myers"
require 'rspec'
require 'rack/test'
require_relative '../app.rb'

describe 'Sinatra App' do
  include Rack::Test::Methods

  def app
    SinatraApp.new
  end

  it "shows the quote of the day" do
    get '/quote'

    expect(last_response.body).to include("Excellence prospers in the absence of excuses")
  end

  it "shows the author of the quote" do
    get '/quote'

    expect(last_response.body).to include("Lorii Myers")
  end
end
